<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> <!-- ETIQUETAS QUE HAY QUE PONER SIEMPRE ARRIBA -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin</title>
</head>
<body>
<%@ include file = "Header.jsp" %>  <!-- TENGO UN FICHERO DE CABECERAS PARA MI APLICACI�N, AS� QUE LO PONGO -->

<h2>New Researcher</h2>
<form action="CreateResearcherServlet" method="post">
        <input type="text" name="id" placeholder="User Id">
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="lastname" placeholder="Last name">
        <input type="text" name="email" placeholder="Email">
        <button type="submit">Create researcher</button>
</form>		

<h2>New Publication</h2>
<form action="CreatePublicationServlet" method="post">
        <input type="text" name="id" placeholder="Publication Id">
        <input type="text" name="publicationname" placeholder="Name">
        <input type="text" name="title" placeholder="Publication Title">
        <input type="text" name="publicationdate" placeholder="Publication Date">
        <button type="submit">Create publication</button>
</form>		

</body>
</html>