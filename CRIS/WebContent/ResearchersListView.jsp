<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> <!-- ETIQUETAS QUE HAY QUE PONER SIEMPRE ARRIBA -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Researchers List</title>
</head>
<body>
<%@ include file = "Header.jsp" %>  <!-- TENGO UN FICHERO DE CABECERAS PARA MI APLICACI�N, AS� QUE LO PONGO -->
	<table border="1">  <!-- TABLA VAC�A CON BORDE -->
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Last name</th>
			<th>Publications</th>
			<th>Email</th>
		</tr>
		<c:forEach items="${researcherslist}" var="ri">  <!-- QUIERO RECORRER NO S� QU� DE INVESTIGADORES, PUES UN FOR Y LOS ATRIBUTOS CON LOS ENLACES -->
			<tr>
				<td><a href="ResearcherServlet?id=${ri.id}">${ri.id}</a></td>
				<td>${ri.name}</td>
				<td>${ri.lastname}</td>
				<td><a href="${ri.scopusURL}">${ri.scopusURL}</a></td>
				<td>${ri.email}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>