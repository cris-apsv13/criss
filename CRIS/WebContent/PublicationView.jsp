<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Publication view</title>
</head>
<body>
	<%@ include file="Header.jsp"%>
	<p style="color: red;">${message}</p>
	<h2>Publication info</h2>
	<p>ID: ${ri.id }</p>
	<p>Name: ${ri.publicationName }</p>
	<p>Title: ${ri.title }</p>
	<p>Date: ${ri.publicationDate }</p>
	<p>Cites: ${ri.citeCount }</p>
	<p>Authors: ${ri.authors }</p>
</body>
</html>